package com.beemsterboerenterprises.iprwc.controller;

import com.beemsterboerenterprises.iprwc.message.request.ProductForm;
import com.beemsterboerenterprises.iprwc.message.request.RemoveProductsForm;
import com.beemsterboerenterprises.iprwc.message.response.HttpResponse;
import com.beemsterboerenterprises.iprwc.model.Product;
import com.beemsterboerenterprises.iprwc.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/product")
public class ProductController {

    @Autowired
    ProductRepository productRepository;

    @PostMapping("/add")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Product> addProduct(@Valid @RequestBody ProductForm productForm){
        return ResponseEntity.ok(productRepository.save(new Product(productForm.getName(), productForm.getImageFile(), productForm.getDescription(), productForm.getPrice(), productForm.getCategories())));
    }

    @GetMapping("/search")
    public ResponseEntity<Set<Product>> searchForProducts(@RequestParam("query") String query){
        if (productRepository.findAllByNameLike(query).isPresent()){
            return ResponseEntity.ok(productRepository.findAllByNameLike(query).get());

        }
        return ResponseEntity.ok(new HashSet<>());
    }

    @GetMapping("/all")
    public ResponseEntity<List<Product>> allProducts() {
        return ResponseEntity.ok(productRepository.findAll());
    }

    @GetMapping("/")
    public ResponseEntity<Product> getProductById(@RequestParam("id") Long id){
        Optional<Product> optOfProduct = productRepository.findById(id);
        if( optOfProduct.isPresent()){
            return ResponseEntity.ok(optOfProduct.get());
        }
        else{
            return ResponseEntity.ok(new Product());
        }
    }
    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/update")
    public ResponseEntity updateProduct(@Valid @RequestBody ProductForm productForm){
        Optional<Product> optOfProduct = productRepository.findById(productForm.getId());
        System.out.println(productForm.getId());
        if(optOfProduct.isPresent()){
            Product loadedProduct = optOfProduct.get();
            loadedProduct.setName(productForm.getName());
            loadedProduct.setDescription(productForm.getDescription());
            loadedProduct.setImageFile(productForm.getImageFile());
            loadedProduct.setPrice(productForm.getPrice());
            productRepository.save(loadedProduct);
            return ResponseEntity.ok("Product updated");
        }
        return ResponseEntity.notFound().build();
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/remove")
    public ResponseEntity<HttpResponse> removeProduct(@RequestBody RemoveProductsForm removeProductsForm){
        for (Long id : removeProductsForm.getIds()
             ) {
            if(productRepository.existsById(id)) {
                productRepository.deleteById(id);
            }
        }
        return ResponseEntity.ok(new HttpResponse("Products deleted"));
    }
}
