package com.beemsterboerenterprises.iprwc.controller;

import com.beemsterboerenterprises.iprwc.message.response.HttpResponse;
import com.beemsterboerenterprises.iprwc.model.User;
import com.beemsterboerenterprises.iprwc.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/api/user")
@RestController
public class UserController {
    @Autowired
    UserRepository userRepository;

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/all")
    public List<User> getUsers(){
        return userRepository.findAll();
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/remove")
    public ResponseEntity<HttpResponse> delete(@RequestParam Long id){
        userRepository.deleteById(id);
        return ResponseEntity.ok(new HttpResponse("User removed") );
    }
}
