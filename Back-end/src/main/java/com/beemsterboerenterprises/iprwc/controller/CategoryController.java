package com.beemsterboerenterprises.iprwc.controller;

import com.beemsterboerenterprises.iprwc.message.request.CategoryForm;
import com.beemsterboerenterprises.iprwc.model.Category;
import com.beemsterboerenterprises.iprwc.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/category")
public class CategoryController {
    @Autowired
    CategoryRepository categoryRepository;

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/add")
    public ResponseEntity<String> addCategory(@Valid @RequestBody CategoryForm categoryForm){
        if (categoryRepository.findByName(categoryForm.getName()).isPresent()){
            return new ResponseEntity<String>("Category already exists!",
                    HttpStatus.BAD_REQUEST);
        }
        categoryRepository.save(new Category(categoryForm.getName()));
        return ResponseEntity.ok().body("Category created");
    }
}
