package com.beemsterboerenterprises.iprwc.controller;

import com.beemsterboerenterprises.iprwc.fileuploads.FileStorageService;
import com.beemsterboerenterprises.iprwc.message.response.HttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@RestController
public class FileController {
    private static final String IMG_FILE_PREFIX = "PRODUCT_IMGID_";
    private static final Logger logger = LoggerFactory.getLogger(FileController.class);
    @Autowired
    private FileStorageService fileStorageService;

    @PostMapping("/api/product/addimage")
    public ResponseEntity<HttpResponse> uploadImageFile(@RequestParam("image")MultipartFile file, @RequestParam("id") Long id){
        String fileName = fileStorageService.storeFile(file, IMG_FILE_PREFIX, id);
        return ResponseEntity.ok(new HttpResponse(fileName));
    }

    @GetMapping("/api/product/image")
    public ResponseEntity<Resource> downloadFile(@RequestParam Long id, HttpServletRequest request) {
        // Load file as Resource
        Resource resource = fileStorageService.loadFileAsResource(IMG_FILE_PREFIX + id);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            logger.info("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }
}

