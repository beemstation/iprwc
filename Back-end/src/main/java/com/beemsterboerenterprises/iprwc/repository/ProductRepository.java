package com.beemsterboerenterprises.iprwc.repository;

import com.beemsterboerenterprises.iprwc.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    Optional<Product> findByName(String name);
    Optional<Set<Product>> findAllByName(String name);
    Optional<Set<Product>> findAllByNameLike(String name);

}
