package com.beemsterboerenterprises.iprwc.fileuploads;

import net.coobird.thumbnailator.Thumbnails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
public class FileStorageService {
    private final Path fileStorageLocation;
    @Autowired
    public FileStorageService(FileStorageProperties fileStorageProperties) {
        this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir())
                .toAbsolutePath().normalize();
        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
        }
    }


    public String storeFile(MultipartFile file, String prefix, Long id) {

        String fileName = StringUtils.cleanPath(prefix + id +"."+ StringUtils.getFilenameExtension(file.getOriginalFilename()));

        try {
            // Check if the file's name contains invalid characters
            if(fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }

            // Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = this.fileStorageLocation.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            Thumbnails.of(targetLocation.toString())
                    .size(128, 128)
                    .toFile(targetLocation.toString());

            return fileName;
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    public Resource loadFileAsResource(String originalFileName) {

            Resource resource = createReasourceFromFileName(originalFileName, ".jpg");
            if(resource.exists()) {
                return resource;
            }
            resource = createReasourceFromFileName(originalFileName, ".png");
        if(resource.exists()) {
            return resource;
        }

        else {
                throw new FileNotFoundException("File not found " + originalFileName);
            }

    }

    private Resource createReasourceFromFileName(String fileName, String ext){
        String newFileName = fileName + ext;
        Path filePath = this.fileStorageLocation.resolve(newFileName).normalize();
        try {
            Resource resource = new UrlResource(filePath.toUri());
            return resource;
        } catch (MalformedURLException e) {
            throw new FileNotFoundException("File not found " + fileName, e);
        }
    }
}
