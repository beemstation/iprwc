package com.beemsterboerenterprises.iprwc.fileuploads;

public class FileStorageException extends RuntimeException {
    public FileStorageException(String s, Throwable cause) {
        super(s, cause);
    }
    public FileStorageException(String message) {
        super(message);
    }
}
