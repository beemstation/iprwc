package com.beemsterboerenterprises.iprwc.message.response;

public class HttpResponse {
    public String message;

    public HttpResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
