package com.beemsterboerenterprises.iprwc.message.request;

import javax.validation.constraints.NotBlank;

public class RemoveProductsForm {
    @NotBlank
    private Long[] ids;

    public RemoveProductsForm() {
    }

    public Long[] getIds() {
        return ids;
    }

    public void setIds(Long[] ids) {
        this.ids = ids;
    }
}
