import { Injectable } from '@angular/core';
import { User } from 'src/app/models/user';
import { BasketItem } from 'src/app/models/basket-item';

@Injectable({
  providedIn: 'root'
})
export class LocalstorageService {
  constructor() { }

  public setUserToken(token: string){
    localStorage.setItem("token", token);
  }

  public getUserToken(){
    return localStorage.getItem("token")
  }

  public setActiveUser(user: User){
    localStorage.setItem("localuser", JSON.stringify(user));
  }

  public getActiveUser(): User{
    return <User> JSON.parse(localStorage.getItem("localuser"))
  }

  public removeUserAndToken(){
    localStorage.removeItem('localuser');
    localStorage.removeItem('token');
  }

  public setBasket(basket: BasketItem[]){
    localStorage.setItem('basket', JSON.stringify(basket))
  }

  public getBasket(): BasketItem[]{
    return JSON.parse(localStorage.getItem('basket'))
  }
}
