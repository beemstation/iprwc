import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Product } from 'src/app/models/product';
import { Observable } from 'rxjs';
import { IdContainer } from 'src/app/models/id-container';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private api: ApiService) { }
  getProducts(): Observable<Product[]>{
    return this.api.get('/api/product/all')
  }

  get(id: number){
    return this.api.get('/api/product/?id='+id)
  }

  update(product: Product){
    console.log(JSON.stringify(product))
    return this.api.post('/api/product/update', product)
  }

  add(product: Product){
    return this.api.post('/api/product/add', product)
  }

  addProductImage(fd: FormData, id: number){
    return this.api.post('/api/product/addimage?id='+id, fd)
  }

  delete(product: Product){
    let idcont : IdContainer = {ids:[product.id]}
    return this.api.post('/api/product/remove', idcont)
  }
}
