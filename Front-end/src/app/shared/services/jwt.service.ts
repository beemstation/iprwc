import { Injectable } from '@angular/core';
import { LocalstorageService } from './localstorage.service';
import { Jwt } from 'src/app/models/jwt';
import { JwtHelperService } from '@auth0/angular-jwt';
import { User } from 'src/app/models/user';

const jwtDecodeService = new JwtHelperService();
@Injectable({
  providedIn: 'root'
})
export class JwtService {

  constructor(private storage: LocalstorageService) { }
  public userFromJWT(): User{
    return this.claimsFromJwt().User
  }

  private claimsFromJwt(): Jwt{
    return jwtDecodeService.decodeToken(this.storage.getUserToken());
  }

  public isExpired(){
    return jwtDecodeService.isTokenExpired(this.storage.getUserToken());
  }
}
