import { Injectable, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs/';
import { Accesstoken } from 'src/app/models/accestoken';
import { environment } from './../../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class ApiService  {
  apiUrl = null;

  constructor(private http: HttpClient){
    this.apiUrl = environment.apiUrl;

      this.apiUrl = '';
    
  }

  public login(username: string, password: string){
    return this.http.post<Accesstoken>(this.apiUrl +'/api/auth/signin', {username: username, password: password})
  }

  public post<T>(url: string, body: Object): Observable<T>{
    return this.http.post<T>(this.apiUrl +url, body)
  }

  public get<T>(url: string): Observable<T>{
    return this.http.get<T>(this.apiUrl +url)
  }

  public delete<T>(url: string): Observable<T>{
    return this.http.delete<T>(this.apiUrl +  url)
  }


}
