import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Accesstoken } from 'src/app/models/accestoken';
import { LocalstorageService } from './localstorage.service';
import { User, UserPrincipal } from 'src/app/models/user';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Jwt } from 'src/app/models/jwt';
import { JwtService } from './jwt.service';
import { Observable } from 'rxjs';
import { UserWithoutRoles } from 'src/app/models/user-without-roles';
import { ToastrService } from 'ngx-toastr';
const jwtDecodeService = new JwtHelperService();

@Injectable({
  providedIn: 'root'
})
export class UserService {
  accessToken: Accesstoken
  constructor(private apiService: ApiService, private storage: LocalstorageService, private toast: ToastrService) { }

  public login(userPrincipal : UserPrincipal){
    return this.apiService.post("/api/auth/signin", userPrincipal);
  }

  public register(user: UserWithoutRoles, rpassword: string){

    return this.apiService.post('/api/auth/signup', user)

  }

  public activeUserIsAdmin(): boolean{
    let roles = this.storage.getActiveUser().roles;
    let admin = false;
    roles.forEach(function(element){
      if (element.name.trim() =='ROLE_ADMIN'.trim()){
        admin = true
      }
    })
    return admin;
  }

  public activeUserPresent(): boolean{
    if (this.storage.getActiveUser()){
      return true
    }
    return false
  }

  public logOut(){
    this.storage.removeUserAndToken()
  }

  public getActiveUserId(): number{
    return this.storage.getActiveUser().id
  }

  public getActiveUser() : User{
    return this.storage.getActiveUser()
  }

  public getUsers(){
    return this.apiService.get('/api/user/all')
  }

  public removeUser(user: User){
    return this.apiService.delete('/api/user/remove?id='+ user.id)
  }

  public registerAdmin(user, rpassword: string){
    this.apiService.post('/api/auth/signupadmin', user)
    .subscribe(data =>
      { 
        this.toast.success('Adminaccount succesvol geregistreerd.')
      }, err => {
        this.toast.error(err.status + ': '+ err.message, 'Er is iets fout gegaan..')
      }
      )
  }
}
