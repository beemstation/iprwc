import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';

import { Observable } from 'rxjs';
import { LocalstorageService } from '../shared/services/localstorage.service';

@Injectable()
export class AuthorizationInterceptor implements HttpInterceptor {
    constructor(private storage:LocalstorageService){}
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    
      if (this.storage.getUserToken()){
        request = request.clone({
          setHeaders: {
            Authorization: `Bearer ${this.storage.getUserToken()}`
          }
        });
      }
    
        return next.handle(request);
      }
}