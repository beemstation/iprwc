export interface UserWithRolesAsStrings {
    id : number
    name : string;
    username: string;
    email : string;
    password : string;
    roles : string[];
}
