export interface Accesstoken {
    tokenType: string,
    accessToken: string
}
