export interface UserWithoutRoles {
        id : number
        name : string;
        username: string;
        email : string;
        password : string;
}
