import { Category } from './category';

export interface Product {
    id: number
    name: string
    imageFile: string
    description: string
    price: number
    categories: Category[]
}
