import { User } from './user';

export interface Jwt {
    User: User
    exp: string
    iat: string
    sub: string
}
