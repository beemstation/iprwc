import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './components/admin/admin.component';
import { ShopComponent } from './components/shop/shop.component';
import { ManageproductsComponent } from './components/manageproducts/manageproducts.component';
import { ManageusersComponent } from './components/manageusers/manageusers.component';
import { CheckoutComponent } from './components/checkout/checkout.component';



const routes: Routes = [
  { path: 'admin', component: AdminComponent},
  { path: '', component: ShopComponent},
  { path: 'manageusers', component: ManageusersComponent},
  { path: 'manageproducts', component: ManageproductsComponent},
  { path: 'shop', component: ShopComponent},
  { path: 'checkout', component: CheckoutComponent}

];
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
