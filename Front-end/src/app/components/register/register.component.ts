import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from 'src/app/shared/services/user.service';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { UserWithoutRoles } from 'src/app/models/user-without-roles';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(public userService: UserService, private formbuilder: FormBuilder, private ngxSmartModalService: NgxSmartModalService, private toast: ToastrService) { }
  registerFormGroup: FormGroup
  ngOnInit() {
    this.registerFormGroup = this.formbuilder.group({
      username: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(100)]],
      repeatPassword: ['', Validators.required],
      name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      email: ['', [Validators.required, Validators.email]]
    })
  }

  public register(){ 
    let user = this.registerFormGroup.value as UserWithoutRoles;
    this.userService.register(user, this.registerFormGroup.get('repeatPassword').value)
    .subscribe(data =>
      { 
        this.ngxSmartModalService.closeLatestModal();
        this.ngxSmartModalService.get('registerModal').close()
        
        this.toast.success('Succesvol geregistreerd!')
      }, err => {
        console.log(err)
        this.ngxSmartModalService.closeLatestModal();
        this.ngxSmartModalService.get('registerModal').close()

        this.toast.error(err.status + ': '+ err.message, 'Er is iets fout gegaan..')
      }
      )
  }

}
