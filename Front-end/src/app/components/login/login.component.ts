import { Component, OnInit } from '@angular/core';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { UserPrincipal } from 'src/app/models/user';
import { UserService } from 'src/app/shared/services/user.service';
import { Router } from '@angular/router';
import { Accesstoken } from 'src/app/models/accestoken';
import { LocalstorageService } from 'src/app/shared/services/localstorage.service';
import { JwtService } from 'src/app/shared/services/jwt.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  password: string
  username: string
  constructor(public ngxSmartModalService: NgxSmartModalService, private userService: UserService, public router: Router, private storage : LocalstorageService, private jwtService: JwtService, private toast: ToastrService) { }

  ngOnInit() {
  }

  public openRegisterModalCloseOtherModals(){
    this.ngxSmartModalService.getModal('loginModal').close()
    this.ngxSmartModalService.getModal('registerModal').open()
  }

  public login(username: string, password: string){
    let userp: UserPrincipal = {
     username: username, password: password
    }

    this.userService.login(userp)
    .subscribe((data: Accesstoken) => {
    this.storage.setUserToken(data.accessToken)
    this.storage.setActiveUser(this.jwtService.userFromJWT())
     
    this.toast.success('Welkom terug ' + this.userService.getActiveUser().name + '!')
    if(this.userService.activeUserIsAdmin()){
      this.ngxSmartModalService.getModal('loginModal').close()
      this.router.navigateByUrl('/admin')
    } else{
      this.ngxSmartModalService.getModal('loginModal').close()
      this.router.navigateByUrl('/')
    }
  }
    
    , err => {
      console.log(err)
    });
    this.ngxSmartModalService.getModal('loginModal').close()


  }

}
