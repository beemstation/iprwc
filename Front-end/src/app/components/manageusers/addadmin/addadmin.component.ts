import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/shared/services/user.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/models/user';
import { UserWithRolesAsStrings } from 'src/app/models/user-with-roles-as-strings';

@Component({
  selector: 'app-addadmin',
  templateUrl: './addadmin.component.html',
  styleUrls: ['./addadmin.component.css']
})
export class AddadminComponent implements OnInit {


  constructor(public userService: UserService, private formbuilder: FormBuilder) { }
  registerFormGroup: FormGroup
  ngOnInit() {
    this.registerFormGroup = this.formbuilder.group({
      username: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]],
      repeatPassword: ['', Validators.required],
      name: ['', Validators.required],
      email: ['', Validators.required]
    })
  }

  public register(){ 
    let user : UserWithRolesAsStrings = this.registerFormGroup.value as UserWithRolesAsStrings
    user.roles = ['admin']
    this.userService.registerAdmin(user, this.registerFormGroup.get('repeatPassword').value)
  }

}
