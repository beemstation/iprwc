import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/shared/services/user.service';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-manageusers',
  templateUrl: './manageusers.component.html',
  styleUrls: ['./manageusers.component.css']
})
export class ManageusersComponent implements OnInit {
  users: User[]
  constructor(private userService: UserService, private ngxSmartModalService: NgxSmartModalService, private router: Router, private toast: ToastrService) { }

  ngOnInit() {
    if(!this.userService.getActiveUser()){
      this.router.navigateByUrl('/')
    } else{
      if(! this.userService.activeUserIsAdmin()){
        this.router.navigateByUrl('/')
      }
    }
    this.userService.getUsers().subscribe(
      (data: User[])=>{
        this.users = data
      }
    )
  }

  removeUser(user: User){
    this.userService.removeUser(user).subscribe(
      data =>{
        this.toast.success('User: ' + user.name + ' removed.')
      }, err =>{
        this.toast.error(err.status + ': '+ err.message, 'Er is iets fout gegaan..')
      }
    )
  }

  addAdminUser(){
    this.ngxSmartModalService.get('registerAdminModal').open()
  }

}
