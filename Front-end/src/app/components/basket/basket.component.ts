import { Component, OnInit, OnChanges } from '@angular/core';
import { Product } from 'src/app/models/product';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { BasketItem } from 'src/app/models/basket-item';
import { BasketService } from './basket.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.css']
})
export class BasketComponent implements OnInit, OnChanges {

  basket: BasketItem[] = []
  constructor(private basketService: BasketService, private router: Router, private ngxSmartModalService: NgxSmartModalService) { }

  ngOnInit() {
    this.basket = this.basketService.basket
  }

  ngOnChanges(){
    this.basket = this.basketService.basket
  }




  toCheckout(){
    this.ngxSmartModalService.get('basketModal').close()
    this.ngxSmartModalService.closeLatestModal();
    this.router.navigateByUrl('checkout');
  }

}
