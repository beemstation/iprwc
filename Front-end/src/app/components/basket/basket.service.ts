import { Injectable } from '@angular/core';
import { BasketItem } from 'src/app/models/basket-item';
import { Product } from 'src/app/models/product';
import { LocalstorageService } from 'src/app/shared/services/localstorage.service';

@Injectable({
  providedIn: 'root'
})
export class BasketService {
  basket: BasketItem[] = []
  totalBasketPrice = [0];
  constructor(private storage: LocalstorageService) { }

  public addToBasket(product: Product, amount: number){
    let alreadyInIt = false;
    this.basket.forEach(basketproduct => {
      if (basketproduct.product === product){
        basketproduct.amount += amount
        alreadyInIt = true;
      }
    });

    if(!alreadyInIt){
    this.basket.push({product: product, amount: amount})

    }
    this.storage.setBasket(this.basket)

  }

}
