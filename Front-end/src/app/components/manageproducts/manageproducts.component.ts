import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Product } from 'src/app/models/product';
import { ProductService } from 'src/app/shared/services/product.service';
import { NgxSmartModalService } from 'ngx-smart-modal'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-manageproducts',
  templateUrl: './manageproducts.component.html',
  styleUrls: ['./manageproducts.component.css']
})
export class ManageproductsComponent implements OnInit {
  public product: Product
  public products: Product[]



  editProductForm: FormGroup

  constructor(private productservice: ProductService, private ngxSmartModalService: NgxSmartModalService, private formbuilder: FormBuilder, private toast: ToastrService) { }

  ngOnInit() {
    this.initEditProductForm()
    this.productservice.getProducts().subscribe(
      (data: Product[]) =>{
        this.products = data
        console.log(this.products);
      
      }, err =>{
        console.log(err)
      }
    )
    console.log(this.products)
  }




  showAddProductForm(){
    this.ngxSmartModalService.getModal('addProductModal').open()
  }

  showEditProductForm(product: Product){
    this.updateEditProductForm(product)
    this.ngxSmartModalService.getModal('editProductModal').open()
  }

  loadProduct(id: number){
    this.productservice.get(id).subscribe(
      (data: Product) =>{
        this.product = data
      }, err =>{
        console.log(err)
      }
    )
  }
  initEditProductForm(){
    this.editProductForm = this.formbuilder.group({
      id: ['', Validators.required],
      name: ['', Validators.required],
      description: ['', Validators.required],
      price: ['', Validators.required],
      imageFile: ['', Validators.required]
    });
    this.editProductForm.get('id').disable()
  }

  
  onEditProductSubmit(){
    if (this.editProductForm.valid){
      this.editProduct()
    }
  }

  updateEditProductForm(product: Product){
    this.editProductForm = this.formbuilder.group({
      id: [product.id, Validators.required],
      name: [product.name, Validators.required],
      description: [product.description, Validators.required],
      price: [product.price, Validators.required],
      imageFile: [product.imageFile, Validators.required]
    });
    this.editProductForm.get('id').disable()
  }

  
  editProduct(){
    let editingProduct: Product = this.editProductForm.value as Product
    this.editProductForm.get('id').enable()
    console.log('edit product called')
    this.productservice.update(editingProduct).subscribe(
      data =>{
        this.toast.success(editingProduct.name + ' aangepast.')
        this.editProductForm.get('id').disable()
        this.ngxSmartModalService.closeLatestModal();
      }, err =>{
        console.log(err)
        this.editProductForm.get('id').disable()
        this.toast.error(err.status + ': '+ err.message, 'Er is iets fout gegaan..')
      }
    )
  }

  deleteProduct(product : Product){
    this.productservice.delete(product).subscribe(
      data =>{
        this.toast.success(product.name + ' uit de webshop verwijderd.')
      }, err =>{
        this.toast.error(err.status + ': '+ err.message, 'Er is iets fout gegaan..')
      }
    )
  }




}