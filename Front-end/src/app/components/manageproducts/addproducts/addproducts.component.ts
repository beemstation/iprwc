import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProductService } from 'src/app/shared/services/product.service';
import { Product } from 'src/app/models/product';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-addproducts',
  templateUrl: './addproducts.component.html',
  styleUrls: ['./addproducts.component.css']
})
export class AddproductsComponent implements OnInit {
  addProductForm: FormGroup
  constructor(private formbuilder: FormBuilder, private productservice: ProductService, private ngxSmartModalService: NgxSmartModalService, private toast : ToastrService) { }

  selectedFile: File = null;
  ngOnInit() {
    this.initAddProductForm()
  }

  initAddProductForm(){
    this.addProductForm = this.formbuilder.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
      price: ['', Validators.required]
    });
  }

  onAddProductSubmit(){
    if (this.addProductForm.valid){
      this.addProduct()
    }
  }

  addProduct(){
    let addingProduct : Product = this.addProductForm.value as Product;
    this.productservice.add(addingProduct).subscribe(
      (data: Product) =>{
        this.toast.success(data.name + ' toegevoegd aan webshop.');
        const fd = new FormData
        fd.append('image', this.selectedFile, this.selectedFile.name)
        this.productservice.addProductImage(fd, data.id).subscribe(
          resp =>{
            
          }, err =>{
            this.toast.error(err.status + ': '+ err.message, 'Er ging iets fout bij het uploaden van de foto.')
          }
        )
        this.ngxSmartModalService.closeLatestModal();

      }, err =>{
        this.toast.error(err.status + ': '+ err.message, 'Er is iets fout gegaan..')
      }
    )
  }

  onFileSelected(event) {
    console.log(event)
    this.selectedFile = <File> event.target.files[0];
  }

}
