import { Component, OnInit } from '@angular/core';
import { NgxSmartModalService } from 'ngx-smart-modal'
import { UserService } from 'src/app/shared/services/user.service';
import { UserPrincipal, User } from 'src/app/models/user';
import { Router } from '@angular/router';
import { LocalstorageService } from 'src/app/shared/services/localstorage.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {



  constructor(public ngxSmartModalService: NgxSmartModalService, public userService: UserService, private toast: ToastrService) { }

  ngOnInit() {
  }
  
  public loggedIn(): boolean{
    return this.userService.activeUserPresent();
  }

  public logOut(){
    this.userService.logOut();
    this.toast.success('Tot ziens!')
  }

  public showBasket(){
    this.ngxSmartModalService.getModal('basketModal').open()
  }

  public userIsAdmin() : boolean{
    return this.userService.activeUserIsAdmin();
  }


}
