import { Component, OnInit, OnChanges } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from 'src/app/shared/services/user.service';
import { User } from 'src/app/models/user';
import { BasketItem } from 'src/app/models/basket-item';
import { LocalstorageService } from 'src/app/shared/services/localstorage.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {

  constructor(private formbuilder: FormBuilder, private userService: UserService, private storage: LocalstorageService, private toast: ToastrService) { }
  checkoutFormGroup: FormGroup;
  productList: BasketItem[];
  totalprice : number
  ngOnInit() {
    this.configureFormGroup()
    this.productList = this.storage.getBasket();
    this.totalprice = this.calculateTotalPrice();
  }



  public configureFormGroup(){
    let activeuser = null
    if (this.userService.getActiveUser()){
    activeuser = this.userService.getActiveUser()
    } else{
      activeuser = {} as User;
      activeuser.name = ''
      activeuser.email = ''
    }
    this.checkoutFormGroup = this.formbuilder.group({
      name: [activeuser.name, Validators.required],
      email: [activeuser.email, [Validators.required, Validators.email]],
      address: ['', Validators.required],
      zipcode: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(6)]],
      phone: ['', [Validators.required, Validators.minLength(10)]]
    })
  }

  public checkout(){
    this.toast.success('Het wordt zo spoedig mogelijk bezorgd.', 'Bedankt voor uw bestelling!')
  }

  private calculateTotalPrice(): number{
    let price = 0
    this.productList.forEach(function(basketitem){
      price = price + basketitem.amount * basketitem.product.price
    })
    return price
  }

  

}
