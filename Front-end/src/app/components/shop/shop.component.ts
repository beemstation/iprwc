import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/models/product';
import { ProductService } from 'src/app/shared/services/product.service';
import { BasketService } from '../basket/basket.service';
import { BasketItem } from 'src/app/models/basket-item';
import { ToastrService } from 'ngx-toastr';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.css']
})
export class ShopComponent implements OnInit {
  products: BasketItem[] = []
  constructor(private productService: ProductService, private basketService: BasketService, private toast: ToastrService) { }

  ngOnInit() {

    this.productService.getProducts().subscribe(
      (data: Product[]) => {

        data.forEach(element => {
          this.products.push({ amount: 1, product: element })
        });

      }, (err: HttpErrorResponse) => {
        this.toast.error(err.status + ': '+ err.message, 'Er is iets fout gegaan..')
      }
    )
    console.log(this.products)
  }

  public addToBasket(product: Product, amount: number) {
    if (amount < 1) {
      this.toast.error('Aantal moet hoger dan 0 zijn!', 'Oh oh..')
    } else {
      this.basketService.addToBasket(product, amount)
      this.toast.success(amount + ' x '+ product.name + ' Toegevoegd aan winkelwagen')
    }
  }

}
